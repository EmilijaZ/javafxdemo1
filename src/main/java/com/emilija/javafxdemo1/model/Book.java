package com.emilija.javafxdemo1.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name = "books")
@Table
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id_book;

    @Column
    private String author;

    @Column
    private String title;

    @Column
    private String ISBN;

    @Column
    private String description;

    public Book(String author, String title, String ISBN, String description) {
        this.author = author;
        this.title = title;
        this.ISBN = ISBN;
        this.description = description;
    }

    public Book(int id_book, String author, String title, String ISBN, String description) {
        this.id_book = id_book;
        this.author = author;
        this.title = title;
        this.ISBN = ISBN;
        this.description = description;
    }
}
