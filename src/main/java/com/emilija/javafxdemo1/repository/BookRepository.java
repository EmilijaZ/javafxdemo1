package com.emilija.javafxdemo1.repository;

import com.emilija.javafxdemo1.model.Book;
import com.emilija.javafxdemo1.service.SessionManager;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.List;

public class BookRepository {

    public static Book getTitleaById_book(int id) {
        Session session = SessionManager.getSessionFactory().openSession();
        Book book = session.find(Book.class, id);
        session.close();
        return book;
    }

    public static List<Book> getBookList() {
        String getAllBooksHQLQuery = "FROM com.emilija.javafxdemo1.model.Book";
        Session session = SessionManager.getSessionFactory().openSession();
        Query selectAllBooksQuery = session.createQuery(getAllBooksHQLQuery);
        List<Book> bookList = selectAllBooksQuery.getResultList();session.close();

       return bookList;
    }

    public static void createBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(book);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public static void deleteBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(book);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public static void updateBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(book);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
}



