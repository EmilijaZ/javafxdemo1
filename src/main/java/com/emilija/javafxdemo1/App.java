package com.emilija.javafxdemo1;

import com.emilija.javafxdemo1.model.Book;
import com.emilija.javafxdemo1.repository.BookRepository;
import com.emilija.javafxdemo1.service.SessionManager;
import javafx.application.Application;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

public class App extends Application {

    private final TableView<Book> tableBooks = new TableView<Book>();
    private final ObservableList<Book> bookList = FXCollections.observableArrayList(BookRepository.getBookList());
    private final HBox booksAddBox = new HBox();

    public static void main(String[] args) {
        App.launch();
    }

    @Override
    public void start(Stage stage) {
        mapVbox(stage);
    }

    @Override
    public void stop() {
        SessionManager.shutdown();
        System.out.println("Application stopped");
    }

    public void mapVbox(Stage stage) {
        System.out.println("Application started");
        addAllBookDataToTable();
        addBook();
        VBox root = new VBox();
        root.getChildren().add(tableBooks);
        root.getChildren().add(booksAddBox);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void addAllBookDataToTable() {
        setTableappearance();
        tableBooks.setEditable(true);

        TableColumn<Book, Integer> column1 = new TableColumn<>("No.");
        column1.setCellValueFactory(
                new PropertyValueFactory<>("id_book"));

        TableColumn<Book, String> column2 = new TableColumn<>("Author");
        column2.setCellValueFactory(
                new PropertyValueFactory<>("author"));
        column2.setCellFactory(TextFieldTableCell.forTableColumn());
        column2.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Book, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Book, String> t) {
                        ((Book) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setAuthor(t.getNewValue());
                    }
                }
        );

        TableColumn<Book, String> column3 = new TableColumn<>("Title");
        column3.setCellValueFactory(
                new PropertyValueFactory<>("title"));
        column3.setCellFactory(TextFieldTableCell.forTableColumn());
        column3.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Book, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Book, String> t) {
                        ((Book) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setTitle(t.getNewValue());
                    }
                }
        );
        TableColumn<Book, String> column4 = new TableColumn<>("ISBN");
        column4.setCellValueFactory(
                new PropertyValueFactory<>("ISBN"));
        column4.setCellFactory(TextFieldTableCell.forTableColumn());
        column4.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Book, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Book, String> t) {
                        ((Book) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setISBN(t.getNewValue());
                    }
                }
        );
        TableColumn<Book, String> column5 = new TableColumn<>("Description");
        column5.setCellValueFactory(
                new PropertyValueFactory<>("description"));
        column5.setCellFactory(TextFieldTableCell.forTableColumn());
        column5.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Book, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Book, String> t) {
                        ((Book) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())
                        ).setDescription(t.getNewValue());
                    }
                }
        );


        tableBooks.getColumns().setAll(column1, column2, column3, column4, column5);
        addDeleteButton();
        addUpdateButton();

        tableBooks.setItems(bookList);
        tableBooks.setPlaceholder(new Label("No rows to display"));
    }

    public void setTableappearance() {
        tableBooks.setPrefWidth(500);
    }

    public void addBook() {
        TextField authorNameLastNameText = new TextField();
        authorNameLastNameText.setPromptText("Enter author");

        TextField titleText = new TextField();
        titleText.setPromptText("Enter title");

        TextField ISBNText = new TextField();
        ISBNText.setPromptText("Enter ISBN");

        TextField descriptionText = new TextField();
        descriptionText.setPromptText("Enter description");

        Button addNewBookButton = new Button("Add");
        booksAddBox.getChildren().addAll(authorNameLastNameText, titleText, ISBNText, descriptionText);
        booksAddBox.getChildren().add(addNewBookButton);

        StringProperty authorNameLastNameTextProperty = authorNameLastNameText.textProperty();
        StringProperty titleTextProperty = titleText.textProperty();
        StringProperty ISBNTextProperty = ISBNText.textProperty();
        StringProperty descriptionTextProperty = descriptionText.textProperty();

        authorNameLastNameTextProperty.addListener((observable, oldValue, newValue) -> {
            authorNameLastNameText.setText(newValue);
        });
        titleTextProperty.addListener((observable, oldValue, newValue) -> {
            titleText.setText(newValue);
        });
        ISBNTextProperty.addListener((observable, oldValue, newValue) -> {
            ISBNText.setText(newValue);
        });
        descriptionTextProperty.addListener((observable, oldValue, newValue) -> {
            descriptionText.setText(newValue);
        });

        addNewBookButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                BookRepository.createBook(new Book(authorNameLastNameText.getText(), titleText.getText(), ISBNText.getText(), descriptionText.getText()));
                System.out.println("new Book added");
                ObservableList<Book> bookList = FXCollections.observableArrayList(BookRepository.getBookList());
                tableBooks.setItems(bookList);
            }
        });
    }

    public void addDeleteButton() {
        TableColumn<Book, Void> button_column = new TableColumn("Delete");
        Callback<TableColumn<Book, Void>, TableCell<Book, Void>> cellFactory = new Callback<TableColumn<Book, Void>, TableCell<Book, Void>>() {
            @Override
            public TableCell<Book, Void> call(final TableColumn<Book, Void> param) {
                final TableCell<Book, Void> cell = new TableCell<Book, Void>() {
                    private final Button button = new Button("Delete");

                    {
                        button.setOnAction((ActionEvent event) -> {
                            Book book = getTableView().getItems().get(getIndex());
                            System.out.println("selected data to delete: " + book);
                            BookRepository.deleteBook(book);
                            ObservableList<Book> bookList = FXCollections.observableArrayList(BookRepository.getBookList());
                            tableBooks.setItems(bookList);
                        });

                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(button);
                        }
                    }
                };
                return cell;
            }
        };
        button_column.setCellFactory(cellFactory);
        tableBooks.getColumns().add(button_column);
    }

    public void addUpdateButton() {
        TableColumn<Book, Void> button_column = new TableColumn();
        Callback<TableColumn<Book, Void>, TableCell<Book, Void>> cellFactory = new Callback<TableColumn<Book, Void>, TableCell<Book, Void>>() {
            @Override
            public TableCell<Book, Void> call(final TableColumn<Book, Void> param) {
                final TableCell<Book, Void> cell = new TableCell<Book, Void>() {
                    private final Button button = new Button("Update");

                    {
                        button.setOnAction((ActionEvent event) -> {
                            Book book = getTableView().getItems().get(getIndex());
                            System.out.println("selected data to update: " + book);
                            BookRepository.updateBook(book);
                            ObservableList<Book> bookList = FXCollections.observableArrayList(BookRepository.getBookList());
                            tableBooks.setItems(bookList);
                        });

                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(button);
                        }
                    }
                };
                return cell;
            }
        };
        button_column.setCellFactory(cellFactory);
        tableBooks.getColumns().add(button_column);
    }
}
