# JavaFxDemo1
 First practical individual project
------------------------------------------
This is a summary of everything I have learned up to this point including:
+ Java Fundamentals & Advanced.
+ Software Testing.
+ MySQL.
+ Hibernate.
+ Java FX.

Used java version "1.8.0_261" and lombok library. MySQL was created by using DBeaver client.
